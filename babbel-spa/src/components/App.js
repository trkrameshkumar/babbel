import React, { Component } from 'react';
import '../styles/App.css';
import { Form, Text } from 'react-form';
import Contact from './contact.js';
import  Actions  from '../actions/index';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class App extends Component {

  render() {

    const searchForm = (
      <Form
        onSubmit={(values) => {
          this.props.DeriveEmailAddress(values.full_name, values.company_domain)
        }}

        validate={({ full_name, company_domain }) => {
          return {
            full_name: !full_name ? 'Full name is required' : undefined,
            company_domain: !company_domain ? 'Company domain is required' : undefined
          }
        }}
      >
      {({submitForm}) => {
        return (
          <form onSubmit={submitForm} className="form-inline">
            <div className="form-group">
              <label htmlFor="exampleInputName">Full name</label>
              <Text field="full_name" type="text" className="form-control" id="exampleInputName" placeholder="Jane Doe"/>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputDomain">company domain</label>
              <Text field="company_domain" type="text" className="form-control" id="exampleInputDomain" placeholder="example.com"/>
            </div>
            <br/>
            <button type="submit" className="btn btn-default">Submit</button>
          </form>
        )
      }}
      </Form>
    )

    return (
      <div className="App">
        { searchForm }
        { this.props.contact_error }
        <Contact />
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    contact_error: state.contact_error
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators( { DeriveEmailAddress: Actions.DeriveEmailAddress }, dispatch )
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

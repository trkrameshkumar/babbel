import React, { Component } from 'react';
import { connect } from 'react-redux';

class Contact extends Component {
	render() {
		return (
			<div>
				{this.props.contact ? <p><b>Derived Email address</b>: {this.props.contact.email} </p> : ""}
			</div>
		)
	}
}

function mapStateToProps(state){
	return {
		contact: state.contact
	}
}

export default connect(mapStateToProps)(Contact)
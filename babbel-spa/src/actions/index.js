import axios from 'axios';
import { API_END_POINT } from '../constants.js';

const Actions = {
  DeriveEmailAddress: (full_name, domain) => {
    return dispatch => {
      axios.get(`${API_END_POINT}/derive_email?full_name=${full_name}&domain=${domain}`)
      .then(function (response) {
        dispatch({
          type: 'EMAIL_DERIVED',
		  payload: response.data
        })
        dispatch({
          type: 'ERROR_INVALID_DOMAIN',
		  payload: null
        })
      })
      .catch(function (error) {
        if(error.response.status === 422){
          dispatch({
	        type: 'EMAIL_DERIVED',
			payload: null
	      })
          dispatch({
            type: 'ERROR_INVALID_DOMAIN',
            payload: `Sorry!: No sample data set available for the domain ${domain}`
          })
        } else {
          console.log(error.response.status);
        }
      });
    };
  }
 }

 export default Actions;
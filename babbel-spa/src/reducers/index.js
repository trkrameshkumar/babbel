import { combineReducers } from 'redux';
import ContactReducer  from './reducer_contact.js';
import ContactErrorReducer from './reducer_contact_error.js';

const rootReducer = combineReducers({
	contact: ContactReducer,
	contact_error: ContactErrorReducer
});

export default rootReducer;
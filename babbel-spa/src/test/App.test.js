import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App.js';
import { shallow } from 'enzyme';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import reducers from '../reducers';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

describe('<App />',()=> {
	it('renders App component', () => { 
	  const component = shallow(
	  	<Provider store={createStoreWithMiddleware(reducers)}>
	  		<App />
	  	</Provider>
	  );
	  expect(component.length).toEqual(1);
	});
});

ENV['RACK_ENV'] = 'test'

require './server.rb'
require 'rspec'
require 'rack/test'


describe 'Derive Email Address API' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  it "returns response 422 for invalid domain" do
    get '/api/derive_email?full_name=john doe&domain=slideshare.net'
    expect(last_response.status).to eq(422)
  end

  it "returns response 200 for valid domain" do
    get '/api/derive_email?full_name=john doe&domain=google.com'
    expect(last_response.status).to eq(200)
  end
end

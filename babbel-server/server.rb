require 'rubygems'
require 'sinatra'
require 'json'
require 'sinatra/cross_origin'


data_set = {
	  "John Doe" => "jdoe@babbel.com",
	  "Arun Jay" => "jayarun@linkedin.com",
	  "David Stein" => "davidstein@google.com",
	  "Mat Lee" => "matlee@google.com",
	  "Marta Dahl" => "mdahl@babbel.com",
	  "Vanessa Boom" => "vboom@babbel.com"
}

def determine_email_pattern(full_name, domain)
	case domain
	when "google.com"
		pattern_first_name_last_name(full_name, domain)
	when "linkedin.com"
		pattern_last_name_first_name(full_name, domain)
	when "babbel.com"
	  	pattern_first_name_initial_last_name(full_name, domain)
	end
end

def pattern_first_name_last_name(full_name, domain)
	full_name = full_name.delete(" ")
	return "#{full_name}@#{domain}"
end

def pattern_last_name_first_name(full_name, domain)
	if full_name.split(" ").count >= 2
	  first_name = full_name.split(" ").first
	  last_name = full_name.split(" ").last
	  email = "#{last_name}#{first_name}@#{domain}"
	else
	  first_name = full_name.split(" ").first
	  email = "#{first_name}@#{domain}"
	end
	return email
end

def pattern_first_name_initial_last_name(full_name, domain)
	if full_name.split(" ").count >= 2
	  first_name_initial = full_name.split(" ").first[0]
	  last_name = full_name.split(" ").last
	  email = "#{first_name_initial}#{last_name}@#{domain}"
	else
	  first_name = full_name.split(" ").first
	  email = "#{first_name}@#{domain}"
	end
	return email
end

get '/api/derive_email' do
	cross_origin
	domain = params['domain']
	full_name = params['full_name']
	contact = data_set.select {|name, email| email.include? params['domain'].to_s}.first
	return status 422 if contact.nil?
	full_name = full_name.to_s.downcase
	email = determine_email_pattern(full_name, domain)
	content_type :json 
	{ email:  email }.to_json
end

get '/' do
	"API app for babbel SPA"
end

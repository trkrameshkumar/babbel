# Things completed on the task:

Ruby `babbel-server`:
1) Created HTTP microservice for API with sinatra

2) wrote rspec test cases to cover the following sinarios
	1) if the entered domain matches with our existing data set 
		return response 200 with derived email address 
	2) return response 422 if the entered domain mismatches with our existing data set

Run this command to check the test cases on ruby: `rspec`

React `babbel-spa`:

1) Created a single page app where the user enteres full_name and domain name and this app will comunicate with API to derive emails from the given input

2) Integrated redux, to handle state of the app

3) Integrated bootstrap css library for basic styles

4) created actions and reducers to handle data with redux

4) Wrote test case to make sure the app componet loads without any issues with redux store

Run this command to check test cases on react: `yarn test`

